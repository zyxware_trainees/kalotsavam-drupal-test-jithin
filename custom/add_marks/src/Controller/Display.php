<?php
/**
 * @file
 * Contains \Drupal\my_custom\Controller\Display.
 */

namespace Drupal\add_marks\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\my_custom\Controller
 */
class Display extends ControllerBase {

  /**
   * showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function showdata() {

// you can write your own query to fetch the data I have given my example.
    $user = \Drupal::currentUser();
    $user_id = $user->id();
    $arguments = array(
      ':uid' => $user_id
    );
    $result = \Drupal::database()->select('add_marks', 'n')
            ->fields('n', array('id', 'chest_no', 'competition', 'marks'))
            ->where('uid=:uid', $arguments, '=')
            ->execute()->fetchAllAssoc('id');
// Create the row element.
    $rows = array();
    foreach ($result as $row => $content) {
      $rows[] = array(
        'data' => array($content->id, $content->chest_no, $content->competition, $content->marks));
    }
// Create the header.
    $header = array('id', 'chest_no', 'competition', 'marks');
    $output = array(
      '#theme' => 'table',    // Here you can write #type also instead of #theme.
      '#header' => $header,
      '#rows' => $rows
    );
    return $output;
  }
}
