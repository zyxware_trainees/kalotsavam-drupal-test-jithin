<?php

namespace Drupal\add_marks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\field\FieldConfigInterface;



/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class custom_simple extends FormBase {

  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $node_array = array();
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'competition');
    $entity_ids = $query->execute();
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
    $competitions = array(); 
    foreach ($nodes as $node) {
      $title[$node->getTitle()] = $node->getTitle();
     }
    //$query = db_select( 'node', 'n' );
    //$query
      //->condition( 'type', 'competition' )
      //->fields( 'n' );
    //$result = $query->execute();
    //$competitions = array();
    //foreach( $result as $row ) {
      //$competitions[ $row->nid ] = $row->nid;
    //}
    //$node = Node::load($competitions);

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t(''),
    ];

    $form['chest_no'] = [
      '#type' => 'textfield',
      '#title' => $this->t('chest_no'),
      '#required' => TRUE,
    ];

    $form['competition'] = [
      '#type' => 'select',
      '#title' => $this->t('competition'),
      '#options' => $title, 
      '#empty_option' => $this->t('-select-'), 
      '#required' => TRUE,
    ];

    $form['marks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('marks'),
      '#required' => TRUE,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'fapi_example_simple_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  //public function validateForm(array &$form, FormStateInterface $form_state) {
    //$title = $form_state->getValue('title');
    //if (strlen($title) < 5) {
      // Set an error for the form element with a key of "title".
      //$form_state->setErrorByName('title', $this->t('The title must be at least 5 characters long.'));
    //}
  //}

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
     * This would normally be replaced by code that actually does something
     * with the title.
     */
    $title = $form_state->getValue('title');
    drupal_set_message(t('You specified a title of %title.', ['%title' => $title]));

      //db insertion

      $user = \Drupal::currentUser();
      $userid = $user->id();
      $Data = db_insert('add_marks')
      ->fields(
      array(
      'chest_no' => $form_state->getValue('chest_no'),
      'competition' => $form_state->getValue('competition'),
      'marks' => $form_state->getValue('marks'),
      'uid' => $userid,
      )
      )->execute();

  }

}
